import React, { Component } from 'react';
import './App.css';
import Dictionaries from './components/dictionaries';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash, faSave, faPen, faExclamationTriangle, faBan, faPlus, faAngleDoubleRight,faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons'

library.add(faPen,faTrash,faSave,faExclamationTriangle,faBan,faPlus,faAngleDoubleRight,faAngleDoubleLeft)

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand mb-0 h1">OneDot</span>
          </nav>
        </header>
        <div className="container mt-4">
          <Dictionaries/>
        </div>
        <footer class="footer">
          <div class="container">
            <span class="text-muted">Marouane Amri </span>
          </div>
        </footer>
      </div>
    );
  }
}

export default App;
