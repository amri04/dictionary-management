import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class Pagination extends Component {

    createPages(){
            let pageCount =  Math.ceil(this.props.itemsCount / this.props.itemsPerPage)
            return Array.from({length: pageCount}, (v, k) => k+1).map(x=>
                <option key={x} vlaue={x}>{x}</option>
            )
    }
    render() {
        return ( 
            <nav aria-label="Page navigation example">
              <ul className="pagination">
                <li>
                    <div className="input-group mb-3">
                        <div className="input-group-prepend"  >
                            <button className="btn btn-dark" 
                                onClick={()=>this.props.onPageChange(this.props.currentPage -1)}
                                disabled={this.props.currentPage===1}>
                                <FontAwesomeIcon icon="angle-double-left" />
                            </button>
                        </div>
                        <select className="form-control" value={this.props.currentPage} name="page" 
                            onChange={(e)=>this.props.onPageChange(parseInt(e.target.value))}>
                            {this.createPages()}
                        </select>
                        <div className="input-group-append">
                            <button className="btn btn-dark" 
                                onClick={()=>this.props.onPageChange(this.props.currentPage+1)} 
                                disabled={this.props.currentPage===Math.ceil(this.props.itemsCount / this.props.itemsPerPage)}>
                                <FontAwesomeIcon icon="angle-double-right" />
                            </button>
                        </div>
                    </div>
                </li>
              </ul>
            </nav>
         );
    }
}
 
export default Pagination;