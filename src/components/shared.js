export const trimDictionary = (dictionary) => {
    return {
        domain: dictionary.domain.trim(),
        range: dictionary.range.trim(),
        id: dictionary.id
    }
}
export const isValid  = (dictionary) => {
    return dictionary.domain.length>0 && dictionary.range.length>0
}