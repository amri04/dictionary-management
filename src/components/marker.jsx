import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Marker extends Component {
    state = {  }
    render() { 
        return ( 
            <span className={"mr-1 badge badge-"+ this.props.severity } role="alert">
                <FontAwesomeIcon className="mr-1" icon="exclamation-triangle"/> 
                {this.props.title} 
            </span>
         );
    }
}
 
export default Marker;