import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Markers from './markers';

class Dictionary extends Component {

    render() { 
        return ( 
            <tr key={this.props.dictionary.id}>
                <td>{this.props.index}</td>
                <td>{this.props.dictionary.domain}</td>
                <td>{this.props.dictionary.range}</td>
                <td>
                    <Markers dictionary={this.props.dictionary} dictionaries={this.props.dictionaries}></Markers>
                </td>
                <td>
                    <button className="btn btn-sm btn-primary mr-2" title="Modify" onClick={() => this.props.onEdit(this.props.dictionary)}>
                        <FontAwesomeIcon icon="pen" />
                    </button>
                    <button className="btn btn-sm btn-danger" onClick={() => this.props.onRemove(this.props.dictionary.id)} title="Remove">
                        <FontAwesomeIcon icon="trash" />
                    </button>
                </td>
            </tr>
        );
    }
}


 
export default Dictionary;