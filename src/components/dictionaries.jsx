import React, { Component } from 'react';
import DictionaryAddForm from './dictionaryAddForm';
import Dictionary from './dictionary'
import DictionaryEditForm  from './dictionaryEditForm';
import Pagination from './pagination';

class Dictionaries extends Component {
    state = { 
        dictionaries:[],
        paginatedDictionaries:[],
        dictionaryToEdit:{},
        dictPerPage:6,
        currentPage: 1
     }

    componentDidMount() {
        if(!localStorage.getItem("lastId"))  localStorage.setItem("lastId",0);
        let dictionaries = this.getDictionaries();
        this.setState({dictionaries},()=>{
            this.loadPage(1);
        })
    }

    createDictionaryTable = () => {    
        return this.state.paginatedDictionaries.map((dictionary,i)=>{
            let index =i+1+((this.state.currentPage-1)*this.state.dictPerPage);
            if(this.state.dictionaryToEdit && dictionary.id === this.state.dictionaryToEdit.id){
                return <DictionaryEditForm key={dictionary.id} index={index} dictionary={dictionary} onUpdate={this.updateDictionary} onCancel={this.selectDicToEdit} dictionaries={this.state.dictionaries}></DictionaryEditForm>
            }
            else{
                return <Dictionary  key={dictionary.id} index={index} dictionary={dictionary} onRemove={this.removeDictionary} onEdit={this.selectDicToEdit} dictionaries={this.state.dictionaries}></Dictionary>
            }
        })
    }

    selectDicToEdit = (dictionary) => {
        this.setState({dictionaryToEdit:dictionary})
    }

    addDictionary = (dictionary) => {
        dictionary.id = this.getUniqueId();
        let dictionaries = [...this.state.dictionaries];
        dictionaries.push(dictionary);
        this.saveDictionaries(dictionaries,()=>{
            this.loadPage(Math.ceil(this.state.dictionaries.length / this.state.dictPerPage)) //goToLastPage
        });        
    }
    getUniqueId(){
        let lastId = parseInt(localStorage.getItem("lastId"));
        lastId++;
        localStorage.setItem("lastId",lastId);
        return lastId;
    }

    removeDictionary = (id) => {
        let dictionaries = this.state.dictionaries.filter(x=>x.id !==id);
        this.saveDictionaries(dictionaries,()=>{
            if(this.state.paginatedDictionaries.length === 1) // last element in page
                 this.loadPage(Math.ceil(this.state.dictionaries.length / this.state.dictPerPage));
            else
            this.loadPage(this.state.currentPage);
        });

    }
    updateDictionary = (dictionary) => {
        let index = this.state.dictionaries.findIndex(x=>x.id === dictionary.id);
        let dictionaries=[...this.state.dictionaries];
        dictionaries[index]=dictionary;
        this.saveDictionaries(dictionaries,()=>{
            this.loadPage(this.state.currentPage);
        });
    }
    loadPage = (page) => {
        let paginatedDictionaries = this.state.dictionaries.slice((page - 1) * this.state.dictPerPage, page * this.state.dictPerPage)
        this.setState({
            paginatedDictionaries,
            currentPage:page
        })
    }
    saveDictionaries(dictionaries,callback){
        this.setState({dictionaries},callback);
        this.saveToLocalStorage(dictionaries);
    }

    saveToLocalStorage = (dictionaries) => {
        localStorage.setItem("dictionaries",JSON.stringify(dictionaries));
    }
    getDictionaries(){
        return  JSON.parse(localStorage.getItem("dictionaries")) || this.setInitialDictionaries();
    }

    setInitialDictionaries = () => {
        let intialDictionaries = [
            {id : this.getUniqueId() ,domain:"Stonegrey",range:"Dark Grey"},
            {id : this.getUniqueId(),domain:"Midnight Black",range:"Black"},
            {id : this.getUniqueId(),domain:"Mystic Silver",range:"Silver"},
        ]
        this.saveToLocalStorage(intialDictionaries);
        return intialDictionaries;
    }

    render() { 
        return (
            <div>
                <h1 className="mb-4">Dictionary Management</h1>
                <table className="table table-striped">
                    <thead className="thead-dark">
                        <tr>
                            <th>#</th>
                            <th>Domain</th>
                            <th>Range</th>
                            <th>Info</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.createDictionaryTable()}
                    </tbody>
                    <tfoot>
                        <DictionaryAddForm onSave={this.addDictionary} dictionaries={this.state.dictionaries}/>
                    </tfoot>
                </table>
                <div className="d-flex justify-content-center">
                    { (this.state.dictionaries.length>this.state.dictPerPage) && <Pagination currentPage={this.state.currentPage} itemsCount={this.state.dictionaries.length} itemsPerPage={this.state.dictPerPage} onPageChange={this.loadPage}></Pagination>}
                </div>
            </div>
        );
    }
}
 
export default Dictionaries;