import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Markers from './markers';
import {isValid,trimDictionary} from './shared';

class DictionaryEditForm extends Component {
    state={
        dictionary: this.props.dictionary
    }
    handleSubmit = () => {
        let dictionary = trimDictionary(this.state.dictionary);
        if(isValid(dictionary)){
            this.props.onUpdate(dictionary);
            this.props.onCancel(null);
        }
    }
    
    handleDomainChange = (event) => {
        let dictionary = {...this.state.dictionary};
        dictionary.domain = event.target.value;
        this.setState({dictionary})
    }
    handleRangeChange = (event) => {
        let dictionary = {...this.state.dictionary}
        dictionary.range = event.target.value;
        this.setState({dictionary})
    }

    render() { 
        return ( 
            <tr>
                <td>{this.props.index}</td>
                <td>
                    <input type="text" placeholder="Domain" className="form-control" value={this.state.dictionary.domain} onChange={this.handleDomainChange} />
                </td>
                <td>
                    <input type="text" placeholder="Range" className="form-control" value={this.state.dictionary.range} onChange={this.handleRangeChange} />
                </td>
                <td>
                {isValid(this.state.dictionary) && <Markers dictionary={trimDictionary(this.state.dictionary)} dictionaries={this.props.dictionaries}></Markers>}
                </td>

                <td>
                    <button className="btn btn-dark btn-sm mr-2" onClick={this.handleSubmit} title="Update" 
                    disabled={!isValid(this.state.dictionary)}>
                        <FontAwesomeIcon icon="save"/>
                    </button>
                    <button className="btn btn-secondary btn-sm" onClick={() => this.props.onCancel(null)} title="Cancel">
                        <FontAwesomeIcon icon="ban"/>
                    </button>
                </td>
            </tr>
        );
    }
}
 
export default DictionaryEditForm;