import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Markers from './markers';
import {isValid,trimDictionary} from './shared';

class DictionaryAddForm extends Component {
    state={
        dictionary:{
            domain:"",
            range:"",
        }
    }
    handleSubmit = () => {
        let dictionary =trimDictionary(this.state.dictionary);
        if(isValid(dictionary)){
            this.props.onSave(dictionary);
            this.setState({dictionary:{domain:"",range:""}});
        }
    }
    handleDomainChange = (event) => {
        let dictionary = {...this.state.dictionary};
        dictionary.domain = event.target.value;
        this.setState({dictionary})
    }
    handleRangeChange = (event) => {
        let dictionary = {...this.state.dictionary}
        dictionary.range = event.target.value;
        this.setState({dictionary})
    }
    render() { 
        return ( 
            <tr>
                <td>
                    <FontAwesomeIcon icon="plus"/>
                </td>
                <td>
                    <input type="text" placeholder="Domain" className="form-control" value={this.state.dictionary.domain} onChange={this.handleDomainChange} />
                </td>
                <td>
                    <input type="text" placeholder="Range" className="form-control" value={this.state.dictionary.range} onChange={this.handleRangeChange} />
                </td>
                <td>
                    {isValid(this.state.dictionary) && <Markers dictionary={trimDictionary(this.state.dictionary)} dictionaries={this.props.dictionaries}></Markers>}
                </td>
                <td>
                    <button className="btn btn-primary" onClick={this.handleSubmit} title="Save" disabled={!isValid(trimDictionary(this.state.dictionary))}>
                        <FontAwesomeIcon icon="plus"/> Add
                    </button>
                </td>
            </tr>
        );
    }
}
 
export default DictionaryAddForm;