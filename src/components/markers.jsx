import React, { Component } from 'react';
import Marker from "./marker";

class Markers extends Component {
    state = {  }
    hasDuplicate = (dictionary,dictionaries) => {
        return dictionaries.some(x=>
            x.id !== dictionary.id && (x.domain === dictionary.domain && x.range === dictionary.range) 
        )
    }
    hasFork = (dictionary,dictionaries) => {
        return dictionaries.filter(x=>
            x.id !== dictionary.id && (x.domain === dictionary.domain && x.range !== dictionary.range) 
        ).length >0
    }
    hasCycle = (dictionary,dictionaries) => {
        return dictionaries.some(x=>
            x.id !== dictionary.id && (x.domain === dictionary.range && x.range === dictionary.domain)
        )
    }
    hasChain = (dictionary,dictionaries) => {
        return dictionaries.some(x=>
            x.id !== dictionary.id && (x.range === dictionary.domain || x.domain === dictionary.range)
        )
    }
    render() { 
        let dictionary = this.props.dictionary;
        let dictionaries = this.props.dictionaries;
        return ( 
            <span>
                {(this.hasCycle(dictionary,dictionaries) && <Marker title="Cycle" severity="danger"></Marker>)
                 || (this.hasChain(dictionary,dictionaries) && <Marker title="Chain" severity="warning"></Marker>)
                }
                {this.hasDuplicate(dictionary,dictionaries) && <Marker title="Duplicate" severity="warning"></Marker>}
                {this.hasFork(dictionary,dictionaries) && <Marker title="Fork" severity="warning"></Marker>}
            </span>
         );
    }
}
 
export default Markers;